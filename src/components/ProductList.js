import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ListView, View, Text, ScrollView, Image, Dimensions } from 'react-native';
import { Actions } from 'react-native-router-flux';
import ProductListItem from './ProductListItem';
import { TopSection, MenuBar, NavBar, ProductCard } from './common';

class ProductList extends Component {

  onNavigatePress() {
    Actions.newsList();
  }

  componentWillMount() {

    this.createDataSource(this.props);
  }

  createDataSource({ products }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.dataSource = ds.cloneWithRows(products);
  }

  renderRow(productItem) {
    return <ProductListItem library={productItem} />;
  }

  render() {
    const filter = ['CATEGORY 1', 'CATEGORY 2'];
    const { 
      textStyle,
      subTextStyle,
      listItemStyle,
      imageStyle,
      nameStyle,
      midSectionStyle,
      pointsStyle,
      leftStyle,
      rightStyle
    } = styles;
    
    return (
      <View style={{ flex: 1 }}>
        <TopSection>
          <MenuBar headerText="MenuBar"/>
          <Text style={ textStyle } onPress={this.onNavigatePress.bind(this)}>Product Information</Text>
          <NavBar filter={filter} />
        </TopSection>
        <ScrollView>
          <ListView
            contentContainerStyle={styles.grid}
            enableEmptySections
            dataSource={this.dataSource}
            renderRow={this.renderRow}
          />
        </ScrollView>
      </View>
    );
  }
}
const styles = {
  grid: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
  },
  textStyle: {
    marginLeft: 20,
    fontSize: 36,
    fontWeight: '400',
    backgroundColor: 'rgba(255, 255, 255, 0)',
    color: '#fafafa',
  },
  subTextStyle: {
    marginLeft: 20,
    fontSize: 24,
    fontWeight: '300',
    color: '#fafafa'
  },
  listItemStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#FF2C5B'
  },
  imageStyle: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 40,
  },
  leftStyle: {
    flexDirection: 'row',
  },
  midSectionStyle: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  nameStyle: {
    fontSize: 14
  },
  rightStyle: {
    justifyContent: 'center',
  },
  pointsStyle: {
    fontSize: 14,
  }
}

const mapStateToProps = state => {
  return { products: state.products };
};

export default connect(mapStateToProps)(ProductList);