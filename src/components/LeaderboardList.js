import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ListView, View, Text, ScrollView, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import LeaderboardListItem from './LeaderboardListItem';
import { TopSection, MenuBar, NavBar, CardSection } from './common';

class LeaderboardList extends Component {

  onNavigatePress() {
    Actions.productList();
  }

  renderLeaderboard() {
    return this.props.leaderboard.map(leaderboardItem => 
      <LeaderboardListItem key={leaderboardItem.id} library={leaderboardItem} />
    );
  }

  render() {
    const filter = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];
    const { 
      textStyle,
      subTextStyle,
      listItemStyle,
      imageStyle,
      nameStyle,
      midSectionStyle,
      pointsStyle,
      leftStyle,
      rightStyle,
      cardSectionStyle,
      cityStyle
    } = styles;

    return (
      <View style={{ flex: 1 }}>
        <TopSection>
          <MenuBar headerText="MenuBar"/>
          <Text style={ textStyle } onPress={this.onNavigatePress.bind(this)}>Leaderboard</Text>
          <Text style={ subTextStyle }>John Doe</Text>
          <NavBar filter={filter} />
        </TopSection>
        <ScrollView>
          {this.renderLeaderboard()}
        </ScrollView>
        <View style={cardSectionStyle}>
            <View style={listItemStyle}>
              <View style={leftStyle}>
                <Image
                  source={{ uri: 'https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-9/13895165_10153813484823951_1178102824663889650_n.jpg?oh=1b4b592bb5b0d27356705dc1cd391287&oe=5934DF0C' }}
                  style={imageStyle}
                />
                <View style={midSectionStyle}>
                  <Text style={nameStyle}>John Doe</Text>
                  <Text style={cityStyle}>Stockholm</Text>
                </View>
              </View>
              <View style={rightStyle}>
                <Text style={pointsStyle}>867</Text>
              </View>
            </View>
          </View>
      </View>
    );
  }
}

const styles = {
  textStyle: {
    marginLeft: 20,
    fontSize: 36,
    fontWeight: '400',
    backgroundColor: 'rgba(255, 255, 255, 0)',
    color: '#fafafa',
  },
  subTextStyle: {
    marginLeft: 20,
    fontSize: 24,
    fontWeight: '300',
    backgroundColor: 'rgba(255, 255, 255, 0)',
    color: '#fafafa',
  },
  cardSectionStyle: {
    borderBottomWidth: 0.5,
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative',
    backgroundColor: '#FF2C5B'
  },
  listItemStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
  },
  imageStyle: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 40,
  },
  leftStyle: {
    flexDirection: 'row',
  },
  midSectionStyle: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  nameStyle: {
    fontSize: 16,
    color: '#fafafa',
    fontWeight: '400'
  },
  cityStyle: {
    fontSize: 14,
    color: '#fafafa',
    fontWeight: '200'
  },
  rightStyle: {
    justifyContent: 'center',
  },
  pointsStyle: {
    fontSize: 14,
    color: '#fafafa',
  }
}

const mapStateToProps = state => {
  return { leaderboard: state.leaderboard };
};

export default connect(mapStateToProps)(LeaderboardList);