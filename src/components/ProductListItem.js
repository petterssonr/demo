import React, { Component } from 'react';
import {
  Text,
  TouchableWithoutFeedback,
  View,
  Image,
  Dimensions
} from 'react-native';
import { ProductCard } from './common';

class ProductListItem extends Component {

  render() {
    const { 
      listItemStyle,
      imageStyle,
      nameStyle,
      gridItem,
      infoStyle
    } = styles;
    const { 
      id, 
      name, 
      info, 
      image
    } = this.props.library;

    return (
      <TouchableWithoutFeedback>
        <View style={gridItem}>
          <ProductCard>
            <Image
              source={{ uri: image }}
              style={imageStyle}
            />
            <Text style={nameStyle}>{name}</Text>
            <Text style={infoStyle}>{info}</Text>
          </ProductCard>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const width = Dimensions.get('window').width;
const styles = {
  gridItem: {
      width: (width / 2),
      height: (width / 2),
      borderWidth: .5,
      borderColor: '#ddd',
      justifyContent: 'center',
      alignItems: 'flex-start',
      backgroundColor: '#fafafa',
  },
  listItemStyle: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  imageStyle: {
    width: 100,
    height: 80,
    marginBottom: 20,
  },
  nameStyle: {
    fontSize: 16,
    fontWeight: '400',
    marginBottom: 5,
  },
  infoStyle: {
    fontSize: 12,
    fontWeight: '200',
    color: '#777'
  }
};

export default ProductListItem;