import React, { Component } from 'react';
import {
  Text,
  TouchableWithoutFeedback,
  View,
  Image
} from 'react-native';
import { CardSection } from './common';

class ListItem extends Component {

  renderDescription() {
    const { description, desc_image } = this.props.library;
    const { descriptionContainerStyle, descriptionStyle, descImageStyle } = styles;

    if (desc_image) {
      return (
        <View style={descriptionContainerStyle}>
          <Text style={descriptionStyle}>
            { description }
          </Text>
          <Image source={{ uri: desc_image }} style={descImageStyle} />
        </View>
      );
    } else if(!desc_image) {
      return (
        <View style={descriptionContainerStyle}>
          <Text style={descriptionStyle}>
            { description }
          </Text>
        </View>
      );
    }
  }

  render() {
    const { 
      titleStyle, 
      dateStyle, 
      monthStyle, 
      dateContainerStyle, 
      textContainerStyle, 
      descriptionStyle,
      titleContainerStyle,
      imageStyle,
      descImageStyle, 
      descriptionContainerStyle
    } = styles;
    const { 
      id, 
      title, 
      date, 
      month, 
      description, 
      image, 
      desc_image 
    } = this.props.library;

    return (
      <TouchableWithoutFeedback>
        <View>
          <CardSection>
            <View style={dateContainerStyle}>
              <Text style={dateStyle}>
                { date }
              </Text>
              <Text style={monthStyle}>
                { month }
              </Text>
            </View>
            <View style={textContainerStyle}>
              <View style= {titleContainerStyle}>
                <Image
                  source={{ uri: image }}
                  style={imageStyle}
                />
                <Text style={titleStyle}>
                  { title }
                </Text>
              </View>
              <View style={descriptionContainerStyle}>
                <Text style={descriptionStyle}>
                  { description }
                </Text>
                {this.renderDescription()}
              </View>
            </View>
          </CardSection>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  dateContainerStyle: {
    padding: 15,
    justifyContent: 'center',
  },
  dateStyle: {
    fontSize: 22,
    fontWeight: '200',
    textAlign: 'center'
  },
  monthStyle: {
    fontSize: 12,
    fontWeight: '200',
    textAlign: 'center'
  },

  textContainerStyle: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10
  },
  titleContainerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 0,
    paddingRight: 10,
    marginBottom: 10
  },
  imageStyle: {
    width: 30, 
    height: 30,
    marginRight: 10,
    borderRadius: 15
  },
  titleStyle: {
    fontSize: 14,
  },

  descriptionContainerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  descriptionStyle: {
    fontSize: 14,
    flex: 3,
  }
};

export default ListItem;