// Import libraries for making a component
import React from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';

const MenuBar = (props) => {
  const { textStyle, viewStyle, hamburgerMenu, searchIcon } = styles;

  return (
    <View style={viewStyle}>
      <TouchableOpacity
        style={styles.hamburgerMenu}
      >
        <Image
          source={require('../../images/menu-button.png')}
          style={{ width: 20, height: 30 }}
        />
      </TouchableOpacity>
      <View style={searchIcon}>
        <Image
          source={require('../../images/search.png')}
          style={{ width: 30, height: 30 }}
        />
      </View>
    </View>
  );
};

const styles = {
  viewStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 60,
    paddingTop: 0,
    elevation: 2,
    position: 'relative'
  },
  hamburgerMenu: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchIcon: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textStyle: {
    fontSize: 16
  }
};

// Make the component available to other parts of the app
export { MenuBar };
