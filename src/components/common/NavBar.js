import React, { Component } from 'react';
import { Text, View, ScrollView } from 'react-native';

class NavBar extends Component {

  renderItem() {
    return this.props.filter.map((item) => {
      return <Text key={ item } style={styles.textStyle}>{ item }</Text>;
    });
  }

  render() {
    return (
      <View style={styles.viewStyle}>
        <ScrollView
          automaticallyAdjustInsets={false}
          horizontal={true}
          decelerationRate={0}
          snapToAlignment="start"
        >
          { this.renderItem() }
        </ScrollView>
      </View>
    );
  };
}

const styles = {
  viewStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    position: 'absolute',
    bottom: 0,
    height: 50,
    paddingTop: 10,
    elevation: 2,
    paddingLeft: 20
  },
  textStyle: {
    backgroundColor: 'rgba(255, 255, 255, 0)',
    fontSize: 14,
    marginRight: 10,
    color: '#fafafa',
  }
};

export { NavBar };
