import React from 'react';
import { View, Dimensions } from 'react-native';

const ProductCard = (props) => {
  return (
    <View style={styles.containerStyle}>
      {props.children}
    </View>
  );
};

const width = Dimensions.get('window').width;

const styles = {
  containerStyle: {
    width: (width / 2),
    height: (width / 2),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    position: 'relative',
  }
};

export { ProductCard };
