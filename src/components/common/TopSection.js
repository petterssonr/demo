import React from 'react';
import { View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const TopSection = (props) => {
  return (
    <LinearGradient 
      colors={['#893178', '#B55482', '#E8808D']}
      start={{x: 0.0, y: 1.0}} 
      end={{x: 0.5, y: 0.25}} 
    >
      <View style={styles.containerStyle}>
        {props.children}
      </View>
    </LinearGradient>

  );
};

const styles = {
  containerStyle: {
    flexDirection: 'column',
    elevation: 1,
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    paddingTop: 20,
    height: 250,
  }
};

export { TopSection };
