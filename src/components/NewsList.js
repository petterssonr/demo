import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ListView, View, Text, ScrollView, Dimensions } from 'react-native';
import { Actions } from 'react-native-router-flux';

import NewsListItem from './NewsListItem';
import { TopSection, MenuBar, NavBar } from './common';

class NewsList extends Component {
  
  onNavigatePress() {
    Actions.leaderboardList();
  }

  renderNews() {
    return this.props.news.map(newsItem => 
      <NewsListItem key={newsItem.id} library={newsItem} />
    );
  }

  render() {
    const filter = ['ALL', 'SALES', 'OFFERS', 'EVENTS'];
    return (
      <View style={{ flex: 1 }}>
        <TopSection>
          <MenuBar headerText="MenuBar"/>
          <Text style={ styles.textStyle } onPress={this.onNavigatePress.bind(this)}>News</Text>
          <NavBar filter={filter} />
        </TopSection>
        <ScrollView>
          {this.renderNews()}
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  textStyle: {
    marginLeft: 20,
    fontSize: 36,
    fontWeight: '400',
    backgroundColor: 'rgba(255, 255, 255, 0)',
    color: '#fafafa',
  }
}

const mapStateToProps = state => {
  return { news: state.news };
};

export default connect(mapStateToProps)(NewsList);