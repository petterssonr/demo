import React, { Component } from 'react';
import {
  Text,
  TouchableWithoutFeedback,
  View,
  Image
} from 'react-native';
import { CardSection } from './common';

class LeaderboardListItem extends Component {

  render() {
    const { 
      listItemStyle,
      imageStyle,
      nameStyle,
      cityStyle,
      midSectionStyle,
      pointsStyle,
      leftStyle,
      rightStyle
    } = styles;
    const { 
      id, 
      username, 
      city, 
      points, 
      image
    } = this.props.library;

    return (
      <TouchableWithoutFeedback>
        <View>
          <CardSection>
            <View style={listItemStyle}>
              <View style={leftStyle}>
                <Image
                  source={{ uri: image }}
                  style={imageStyle}
                />
                <View style={midSectionStyle}>
                  <Text style={nameStyle}>{username}</Text>
                  <Text style={cityStyle}>{city}</Text>
                </View>
              </View>
              <View style={rightStyle}>
                <Text style={pointsStyle}>{points}</Text>
              </View>
            </View>
          </CardSection>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  listItemStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15
  },
  imageStyle: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 40,
  },
  leftStyle: {
    flexDirection: 'row',
  },
  midSectionStyle: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  nameStyle: {
    fontSize: 16,
    fontWeight: '400'
  },
  cityStyle: {
    fontSize: 14,
    color: '#777',
    fontWeight: '200'
  },
  rightStyle: {
    justifyContent: 'center',
  },
  pointsStyle: {
    fontSize: 14,
  }
};

export default LeaderboardListItem;