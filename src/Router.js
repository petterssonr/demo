import React from 'react';
import { StatusBar, View } from 'react-native';
import { Scene, Router, Actions } from 'react-native-router-flux';

import NewsList from './components/NewsList';
import LeaderboardList from './components/LeaderboardList';
import ProductList from './components/ProductList';

StatusBar.setBarStyle('light-content', true);

const RouterComponent = () => {
  return (
    <Router sceneStyle={{ paddingTop: 0 }}>
      <Scene key="main">
        <Scene
          key="newsList"
          component={NewsList}
          title="News"
          initial
          // navigationBarStyle={{opacity: 0.2}}
          hideNavBar={true}
        />
        <Scene 
          key="leaderboardList" 
          component={LeaderboardList} 
          title="Leaderboard" 
          hideNavBar={true}
        />
        <Scene 
          key="productList" 
          component={ProductList} 
          title="Product Information" 
          hideNavBar={true}
        />
      </Scene>
    </Router>
  );
};

export default RouterComponent;