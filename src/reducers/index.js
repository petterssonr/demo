import { combineReducers } from 'redux';
import NewsReducer from './NewsReducer';
import LeaderboardReducer from './LeaderboardReducer';
import ProductReducer from './ProductReducer';

export default combineReducers({
  news: NewsReducer,
  leaderboard: LeaderboardReducer,
  products: ProductReducer
});
